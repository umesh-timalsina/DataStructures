/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtStack;

/**
 *
 * @author tumesh
 * @param <T>
 */
public class ArrayStack<T> implements Stack<T>{
        public static final int CAPACITY = 1024;
        private int N;
        private T [] S;
        private int t = -1;
        
        
        // A bunch of constructor methods for class Array Stack
        public ArrayStack(){
            this(CAPACITY);
        }
        
        public ArrayStack(int cap){
            N = cap;
            S = (T [])new Object[N];
        }
        
        
        public int getT(){
            return this.t;
        }
        
        
        
        @Override
        public int size(){
            return t+1;
        }
        
        
        @Override
        public boolean isEmpty(){
            return(t<0);
        }
        
         
        @Override
        public void push(T element) throws StackFullException{
            if(t==(N-1)){
                throw new StackFullException("The Stack is full");
            }
            //System.out.println("Flow comes here");
            S[++t] = element;
        }
        
        
        @Override
        public T top() throws StackEmptyException{
            if(this.isEmpty()){
                throw new StackEmptyException("The Stack is empty");
            }
            return  S[t];
        }
        
        @Override
        public T pop() throws StackEmptyException{
            T elem;
            if(this.isEmpty()){
                throw new StackEmptyException("The Stack is empty");
            }
            elem = S[t];
            S[t--] = null;
            return  elem;
        }     
        public T[] toArray(){
            T [] temp = (T [])new Object[CAPACITY];
            int size = this.size();
            for(int i=0; i<size; i++){
                temp[i] = this.pop();
            }
            for(int i=0; i < size; i++){
                this.push(temp[i]);
            }
            return temp;
        }
}
