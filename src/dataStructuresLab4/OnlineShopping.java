/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab4;

/**
 *
 * @author utimalsina
 */
public abstract class OnlineShopping<T> {
   
    public abstract boolean addItem(T anItem);
    public abstract T removeItem();
    public abstract int getCurrentSize();
}
