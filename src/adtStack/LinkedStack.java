/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtStack;

/**
 *
 * @author robotics
 * @param <T>
 */
public class LinkedStack<T> implements Stack<T>{

    private Node topNode;
    private int size=0;
    
    public LinkedStack(){
        this.topNode = null;
    }
    
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (size==0);
    }

    @Override
    public T top() throws StackEmptyException {
        if(!isEmpty()) return this.topNode.data;
        else throw new StackEmptyException("Stack is empty");
            
    }

    @Override
    public void push(T element) {
        Node newNode = new Node(element, topNode);
        this.topNode = newNode;
        this.size++;
    }

    @Override
    public T pop() throws StackEmptyException {
        T returnValue = null;
        if (this.topNode!=null){
            returnValue = this.topNode.data;
            this.topNode = this.topNode.next;
        }
        this.size--;
        return returnValue;
    }
    
    public LinkedStack cut(){
        LinkedStack temp = new LinkedStack();
        while(!this.isEmpty()){
            temp.push(this.pop());
        }
        return temp;
    }// returns another linked stack
    
    public T peek2(){
        T temp = this.pop();
        T returnValue= this.top();
        this.push(temp);
        return returnValue;
    }// Assignment 4 question 1
    
    public T [] toArray(){
        T [] temp = (T []) new Object[this.size()];
        int index = 0;
        int count = this.size();
        while(!this.isEmpty() && index < this.size()){
            temp[index] = this.pop();
            index++;
        }
        for(int i = 0; i < count; i++){
            this.push(temp[i]);
        }
        return temp;
    } // Assignment 4 question 1.
    private class Node{
        public T data;
        public Node next;
        
        public Node(T data){
            this(data, null);
        }
        public Node(T data, Node next){
            this.data = data;
            this.next = next;
        }
    }
    
}
