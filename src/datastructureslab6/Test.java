/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructureslab6;

/**
 *
 * @author robotics
 */
public class Test {
    
    public static void print(BagInterface bag1){
        Object [] arrBag = new Object[bag1.getCurrentSize()];
        arrBag = bag1.toArray();
        for(Object Entry : arrBag){
            System.out.println(Entry.toString());
        }
    }
    
    public static void displayArray(int array[], int first, int last){
        if (first == last)
            System.out.print(array[first] + " ");
        else{
            int mid = (first + last) / 2;
            displayArray(array, mid + 1, last);
            displayArray(array, first, mid);
            
        } // end if
    } // end displayArray
    
    public static void main(String[] args) {
        SinglyLinkedList<Integer> integers = new SinglyLinkedList<>();
        integers.add(5);
        integers.add(6);
        integers.add(7);
        integers.add(8);
        System.out.println(integers.findSecondMax());
        System.out.println(integers.findMax());
        System.out.println(integers.findMin());
        System.out.println(integers.findAvg());
        displayArray(new int [] {2,3,4,5,6}, 0, 4);
    }
}
