
package assignment6;


/** An array based implementation of priority queue
 * @author umesh timalsina
 * @param <T> is the generic comparbale object.
 */
public class ArrayPriorityQueue<T extends Comparable<? super T>> 
                                        implements PriorityQueueInterface<T>{

    private T[] priorityQueue; 
    private int numberOfElements; // provides back index for the queue
    public static final int DEFAULT_INITIAL_CAPACITY = 50;
    
    
    public ArrayPriorityQueue(){
        this(DEFAULT_INITIAL_CAPACITY);
    }// end ArrayPriorityQueue
    
    public ArrayPriorityQueue(int initialCapacity){
        @SuppressWarnings("unchecked")
        T[] temp;
        temp = (T[]) new Comparable [initialCapacity];
        this.priorityQueue = temp;
        this.numberOfElements = 0;
    }
    
    
    @Override
    public void add(T newEntry) {
        ensureCapacity();
        int index = findIndex(newEntry);
        if(!(this.priorityQueue[index] == null)){
            shiftArray(index);
        }
        this.priorityQueue[index] = newEntry;
        this.numberOfElements++;
    }// end add

    @Override
    public T remove() {
        T topElement = null;
        if(!this.isEmpty()){
            topElement = this.priorityQueue[this.numberOfElements-1];
            this.priorityQueue[this.numberOfElements-1] = null;
            this.numberOfElements--;
        }// end if
        return topElement;
    }// end remove

    @Override
    public T peek() {
        T topElement = null;
        if(!this.isEmpty()){
            topElement = this.priorityQueue[this.numberOfElements-1];
        }// end if
        return topElement;
    }// end peek

    @Override
    public boolean isEmpty() {
        return this.numberOfElements == 0;
    }

    @Override
    public int getSize() {
        return this.numberOfElements;
    }

    @Override
    public void clear() {
        for(int index = 0; index < this.numberOfElements; index++){
            this.priorityQueue[index] = null;
        }// end for 
    }// end clear
    
    
    /* A set of private methods for finding array index and shifting the array*/
    
    /* Find index according to priority*/
    private int findIndex(T anEntry){
        int index = 0;
        for(int i = 0; i < this.numberOfElements; i++){
            if(this.priorityQueue[i] == null){
                index = i;
                break;
            } // empty position
            else if(anEntry.compareTo(this.priorityQueue[i]) < 0) index++; // keep on searching
            else index = i; 
        }// end for
        return index;
    }// end findIndex
    
    /* ensure Capacity method */
    private void ensureCapacity(){
        if((this.priorityQueue.length-1) == this.numberOfElements){
           @SuppressWarnings("unchecked")
           T [] tempArray = (T[]) new Comparable[this.priorityQueue.length*2];
           System.arraycopy(this.priorityQueue, 0, tempArray, 
                                        0, this.priorityQueue.length);
           this.priorityQueue = tempArray;
        }// end if
    }// end ensureCapacity
    
    private void shiftArray(int start){
        ensureCapacity();
        for(int i = this.numberOfElements; i >= start; i--){
            this.priorityQueue[i] = this.priorityQueue[i-1];
        }// end for
    }// end shiftArray
    
}
