/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab4;

/**
 *
 * @author utimalsina
 */
public class genericMethods {
   
    public static<T> void printArray(T [] arr){
        for(T t : arr){
            System.out.println(t);
        }
    }
    public static <T> T compareObjects(T val1, T val2){
        if(val1.equals(val2)){
            return val1;
        }// Return the first value if the values are equal
        else{
            return null;
        } // return null if the values are not equal
    }
    public static void main(String[] args) {
        Character [] arr = {'a', 'b', 'c', 'd'};
        printArray(arr);
        
        Character retValue = compareObjects('A', 'B'); // calling generic method
        System.out.println(retValue);
        Integer retVal = compareObjects(1, 1); // same method for integer
        System.out.println(retVal);
        
    }
}
