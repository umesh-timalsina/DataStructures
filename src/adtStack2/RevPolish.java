/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtStack2;
import adtStack.*;
/** The method infixToPostfix in the class converts the an infix expression to 
 * a postfix expression
 * @author tumesh
 */
public class RevPolish {
    
//    public static String convertToPostfix(String infix){
//        String postfix = new String();
//        Stack<Character> operatorStack = new ArrayStack<>(25); // Creates a stack of capacity 1024
//        Character nextChar = ' ';
//        int charCount = infix.length();
//        for(int index = 0; index < charCount; index++){
//            nextChar = infix.charAt(index);
//            System.out.println(nextChar);
//            switch(nextChar){
//                case '^':
//                    operatorStack.push(nextChar);
////                    System.out.println("pushed ^");
//                    break;
//                case '+': case '-': case '*': case '/':
//                    while(!operatorStack.isEmpty() && 
//                            checkPrecedance(nextChar, operatorStack.top())){
//                        postfix = 
//                               postfix.concat((operatorStack.top()).toString());
//                        operatorStack.pop();
//                    }
////                    System.out.println("Pushed" + nextChar);
//                    operatorStack.push(nextChar);
//                    break;
//                case '(':
////                    System.out.println("Found a starting paranthesis");
//                    operatorStack.push(nextChar);
//                    break;
//                case ')':
//                    Character topOperator = operatorStack.pop();
////                    System.out.println(topOperator);
//                    while(!topOperator.equals('(')){
////                        System.out.println("Popped " + operatorStack.top());
//                        postfix = postfix.concat(topOperator.toString());
//                        topOperator = operatorStack.pop();
//                    }
//                    break;
//                default:
//                    postfix = postfix.concat(nextChar.toString());
//                    break;
//            }
//        }
//

    
    public static String convertToPostFix(String infix){
        String postfix = "";
        Stack<Character> operatorStack = new ArrayStack<>();
        int charCount = infix.length();
        int index = 0;
        Character nextChar;
        for(index=0; index < charCount; index++){
            nextChar = infix.charAt(index);
            switch(nextChar){
                case '^':
                    operatorStack.push(nextChar);
                    break;
                case '+': case '-': case '*': case '/':
                    while(!operatorStack.isEmpty() && 
                            checkPrecedance(nextChar, operatorStack.top())){
                        postfix = postfix.concat(operatorStack.top().toString());
                        System.out.println("I have been here");
                        operatorStack.pop();
                    }
                    operatorStack.push(nextChar);
                    break;
                case '(':
                    System.out.println("Found opening parenthesis");
                    operatorStack.push(nextChar);
                    break;
                case ')':
                    Character topElement = operatorStack.pop();
                    while(topElement != '(' && !operatorStack.isEmpty()){
                        postfix = postfix.concat(topElement.toString());
                        topElement = operatorStack.pop(); //Problem here
                    }
                    break;
                default:
                    postfix = postfix.concat(nextChar.toString());
                    
                    break;
            }
        }
        while(!operatorStack.isEmpty()){
            postfix = postfix.concat(operatorStack.pop().toString());
        }
        return postfix;
    }
    private static boolean checkPrecedance(Character op1, Character op2){
        return(!((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-')));
        
    }
    public static void main(String[] args) {
        System.out.println(convertToPostFix("(A*(B-C)+D^E)"));
    }
}
