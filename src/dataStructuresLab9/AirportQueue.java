/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab9;

import java.util.ArrayDeque;

/**
 *
 * @author utimalsina
 */
public class AirportQueue {
    private ArrayDeque<Airplane> airQueue;
    private ArrayDeque<Airplane> landQueue;
    
    public AirportQueue(){
        //reset();
        airQueue = new ArrayDeque<>(50);
        landQueue = new ArrayDeque<>(50);
    }
    
    public boolean addPlane(Airplane plane){
        if(plane.getIsInAir()){
            airQueue.addLast(plane);
        }
        else{
            landQueue.addLast(plane);
        }
        
        return true;
    }
    
    public Airplane landPlane(){
        Airplane land = this.airQueue.removeFirst();
        this.landQueue.addLast(land);
        land.setIsInAir(false);
        return land;
    }
    
    public Airplane takeOff(){
        Airplane landPlane, takeOffPlane;
        while(!this.airQueue.isEmpty()){
           landPlane = airQueue.removeFirst();
           this.landQueue.addLast(landPlane);
           landPlane.setIsInAir(false);
        }
        takeOffPlane = this.landQueue.removeFirst();
        takeOffPlane.setIsInAir(true);
        
        return takeOffPlane;
    }
    
    
    private void reset(){
        airQueue.clear();
        landQueue.clear();
    }
}
