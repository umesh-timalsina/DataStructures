/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab5;

/**
 *
 * @author utimalsina
 */
public class Test {

    public static void main(String[] args) {
        DoublyLinkedWallet<Double> wallet1= new DoublyLinkedWallet();
        wallet1.addNote(50.0);
        wallet1.addNote(60.0);
        wallet1.addNote(65.0);
        wallet1.addNote(65.0);
        wallet1.addNote(8.90);
        wallet1.print();
        System.out.println(wallet1.getTotalAmount());
        wallet1.removeNote(8.9);
        wallet1.print();
        System.out.println("The element 65.0 is in index " + wallet1.find(65.0));
        
        
    }
}
