/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment6;

/**
 *
 * @author robotics
 */
public class MyInteger implements Comparable<MyInteger>{

    public int value;

    public MyInteger(int value) {
        this.value = value;
    }
    
    @Override
    public int compareTo(MyInteger o) {
        if(this.value > o.value) return -1;
        else return 1;
    }
    @Override
    public String toString(){
        return Integer.toString(this.value);
    }
    
}
