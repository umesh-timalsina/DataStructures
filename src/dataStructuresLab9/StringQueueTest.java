/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab9;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 *
 * @author utimalsina
 */
public class StringQueueTest {
    public static void main(String[] args) {
        // Overriding String Comparisons
        
        PriorityQueue<ComparableStrings> pq1 = new PriorityQueue();
        ComparableStrings s1 = new ComparableStrings("Dog");
        ComparableStrings s2 = new ComparableStrings("Kitten");
        ComparableStrings s3 = new ComparableStrings("Nepal Ko Choro");
        pq1.add(s1);
        pq1.add(s2);
        pq1.add(s3);
        //System.out.println(comparator.compare("Dog", "Kitten"));
        
        PriorityQueue<String> pq2 = new PriorityQueue<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if(o1.length()>o2.length()){
                    return 1;
                }
                else if(o1.length() == o2.length()) return 0;
                else return -1;
            }
        });
        pq2.add("Hello");
        pq2.add("Hi");
        pq2.add("Bye");
        
        System.out.println(pq2.peek());
    }

    
    
    
}
