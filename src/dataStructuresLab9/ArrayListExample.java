/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab9;

import java.util.ArrayList;

/**
 *
 * @author utimalsina
 */
public class ArrayListExample {
    public static void main(String[] args) {
        ArrayList<String> list1 = new ArrayList();
        list1.add(0, "Ram");
        list1.add(1, "Sam");
        list1.add(2, "Hari");
        list1.add(3, "Geeta");
        list1.remove(0);
        list1.remove("Sam");
        list1.add("John");
        int id = list1.indexOf("John");
        //list1.add(id, "Jack");
        list1.set(id, "Jack");
        System.out.println(list1.size());
             
    }
}
