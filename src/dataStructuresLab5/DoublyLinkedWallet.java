/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab5;

/**
 *
 * @author utimalsina
 * @param <T>
 */
public class DoublyLinkedWallet<T> implements WalletInterface<T>{
    
    private Node<T> firstNode;
    private Node<T> lastNode;
    private Node<T> currentNode;
    private int numberOfEntries = 0;
    @Override
    public double getTotalAmount() {
        Double totalAmount = 0.0;
        this.currentNode = this.firstNode;
        while(this.currentNode != null){
            totalAmount += Double.parseDouble(this.currentNode.getData().toString());
            this.currentNode = this.currentNode.getNext();
        }// returning string
        return totalAmount;
    } // Total Amount returend
    @Override
    public void removeNote() {
        this.currentNode = firstNode;
        if(this.firstNode == null) {
            System.out.println("List is empty");
        }
        else{
            //System.out.println("Removed " + this.current.getData());
            this.firstNode = this.currentNode.getNext();
            this.currentNode.setPrevNode(null);
        }
    }
    @Override
    public double foriegnTotalAmount(double exchangeRate) {
        double amount = this.getTotalAmount();
        return amount*exchangeRate;
    }
    public void print(){
        this.currentNode = this.firstNode;
        do{
            System.out.println(currentNode.getData());
            this.currentNode = this.currentNode.getNext();
        } while(currentNode != null);
    
    }// Prints the items of the linked list
    @Override
    public void addNote(T aNote){
        Node<T> newNode = new Node(aNote);
        if(firstNode ==  null){
            firstNode = newNode;
            newNode.prev = null;
            newNode.next = null;
            lastNode = firstNode;
            numberOfEntries++;
            System.out.println("First Node added");
        } // If the node is empty, add the the very first
        else{
            currentNode = firstNode;
            while(currentNode.getNext() != null){
                  
                currentNode = currentNode.getNext();
            }
            this.currentNode.setNextNode(newNode);
            newNode.setNextNode(null);
            newNode.setPrevNode(currentNode);
            this.lastNode = newNode;
            numberOfEntries++;
            System.out.println("Added something from here");
        }// The current node is the last node. Its next node is set to new node.
    }
    public int find(T aNote){
        this.currentNode = this.firstNode;
        int index = 0;
        while(this.currentNode != null){
            
            if(currentNode.getData().equals(aNote)){
                break;
            }
            else{
                this.currentNode = this.currentNode.getNext();
            }
            index++;
        }
        if(index == this.numberOfEntries){
            System.out.println(numberOfEntries);
            return -1;
        }
        else{
            return index;
        }
    }
    @Override
    // Copy the contents of the first node to the node which is to be removed, 
    // then remove the first node.
    public void removeNote(T aNote){
        this.currentNode = this.firstNode; // Start From the first node
        while(this.currentNode != null){
            if(this.currentNode.getData().equals(aNote)){
                System.out.println("Removed "  + this.currentNode.getData());
                this.copyNodeContents(this.firstNode, this.currentNode);
                this.removeNote();
                break;
            }// copy the contents of the first node to the current node, and remove the first node
            else{
                this.currentNode = this.currentNode.getNext();
            }// Go to the next element if not found    
        }
        if(this.currentNode == null){
            System.out.println("The element is not in the wallet");
        }
        
    }
    private void copyNodeContents(Node sNode, Node dNode){
        dNode.setData(sNode.getData());
    }
    private class Node<T>{
        private T data;
        private Node prev;
        private Node next;
        
        public Node(T dataPortion){
            this(dataPortion, null, null);
        
        }
        
        public Node(T dataPortion, Node prev, Node next){
            this.data = dataPortion;
            this.prev = prev;
            this.next = next;
        }
        
        public void setNextNode(Node entry){
            this.next= entry;
        }
        public void setPrevNode(Node entry){
            this.prev = entry;
        }

        public Node getPrev() {
            return prev;
        }

        public Node getNext() {
            return next;
        }
        public T getData(){
            return this.data;
        }
        
        public void setData(T anEntry){
            this.data = anEntry;
        }
    
    }
}
