/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author robotics
 */
public class Test {
    public static void main(String[] args) {
        System.out.println(example(6));
        System.out.println(checkPalindrome("LEVEL"));
        decToBin(6);
    }
    
    public static int example(int n){
        if (n <= 0)
            return 0;
        else
            return example(n-3) + n + example(n-2) + n;
    }
    public static boolean checkPalindrome(String input){
        int first = 0, last = input.length()-1;
        boolean isPalindrome=true;
        if(input.length()<=1)
            return isPalindrome;
        else{
            if(input.charAt(first) == input.charAt(last)){
                input = input.substring(first+1, last-1);
                checkPalindrome(input);
            }
            else
                isPalindrome = false;
        }
        return isPalindrome;
    }
    
    public static void decToBin(int n){
        if(n>=1){
            decToBin(n/2);
            int rem = n%2;
            System.out.print(rem);   
        }// Recursive call happens at first
    }
}
