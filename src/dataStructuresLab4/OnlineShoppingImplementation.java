/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab4;

/**
 *
 * @author utimalsina
 */
public class OnlineShoppingImplementation<T> extends OnlineShopping<T> {
    
    private T [] items;
    private static final int DEF_CAP = 30;
    private int numberOfEntries;
    
    public OnlineShoppingImplementation(int capacity){
         this.items = (T []) new Object [capacity];
         this.numberOfEntries = 0;
    }   
    
    public OnlineShoppingImplementation(){
        this(DEF_CAP);
    }
    
    private void resizeIfFull(){
        if(this.numberOfEntries == this.items.length){
            T [] tempBag = (T [])new Object[this.items.length *2];
            for(int i = 0; i<this.items.length; i++){
                tempBag[i] = this.items[i];
            }
            this.items = tempBag;
        }
    }// A private helper method to resize the array if it is full
    
    @Override
    public boolean addItem(T anItem){
        this.resizeIfFull();
        this.items[this.numberOfEntries] = anItem;
        this.numberOfEntries++;
        return true;
    }// Adds anItem to the shopping bag
    
    @Override
    public T removeItem(){
        this.numberOfEntries--;
        T temp = this.items[this.numberOfEntries];
        this.items[this.numberOfEntries] = null;
        return temp;
    }// Removes the last item from the bag
    
    @Override
    public int getCurrentSize(){
        return this.numberOfEntries;
    } // Gives the current size of the online shopping
    
    public boolean contains(T anItem){
        boolean found = false;
        for(int i =0; !found && i<this.numberOfEntries ; i++){
            if(anItem.equals(this.items[i])) found = true;
        }
        return found;
    }// returns true if anItem is in the shopping bag
}
