/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab2;

/**
 *
 * @author utimalsina
 */
public class Student {
    
    // attributes of the Name class
    private Name fullName;
    private String studentID;
    
    
    // Hooking up student with a name and an ID
    public void setStudent(Name nameIn, String studentIDIn){
        this.fullName = nameIn;
        this.studentID = studentIDIn;
    }
    
    // a get method for returning name as a string
    public String getName(){
        return this.fullName.getName();
    }
    
    // a set method for studentID
    public void setStudentID(String studentID){
        this.studentID = studentID;
    }
    
    public String getStudentID(){
        return this.studentID;
    }
    
    
}
