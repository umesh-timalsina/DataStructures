/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment1Scribbles;

/** A generic interface that describes the operation of the ADT Matrix. 
 * @author tumesh
 * @param <T>
 */
public interface MatrixInterface<T> {

    /** Get the dimensions of the matrix.
     * @return an integer array with two elements, first being the row and 
     *         second being the column
     */
    public int [] getDims();

    /** Find the transpose of the given matrix.
     * @return an the transpose of the matrix
     */
    public T transpose();

    /** Multiply the matrix by a scalar quantity
     * @param scalarQuantity is a double value that should be multiplied 
     *                       to the matrix
     * @return a matrix object as a result of the scalar multiplication
     */
    public T mulScalar(double scalarQuantity);

    /** Add the matrix object with another matrix object
     * @param Matrix2 is the Matrix to which the object is to be added
     * @return A Matrix object(as a result of addition) if the matrices 
     *         are addable, null otherwise
     */
    public T add(T Matrix2);

    /** Multiply the matrix object with another matrix
     * @param Matrix2 is the Matrix to which the object is to be multiplied
     * @return A Matrix object(as a result of multiplication) if the matrices 
     *         are multipliable, null otherwise
     */
    public T mul(T Matrix2);
    
}
