/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment6;

/**
 *
 * @author robotics
 */
public class ArrayQueue<T> implements QueueInterface<T> {

    private T[] queue;
    private int frontIndex;
    private int backIndex;
    public static final int DEFAULT_INITIAL_CAPACITY = 50;
    
    /** A default constructor for this class*/
    public ArrayQueue(){
        this(DEFAULT_INITIAL_CAPACITY);
    }// end ArrayQueue
    
    /** A constructor to create an Array Queue with given initial capacity
     * @param initialCapacity initial capacity of the queue to be created
     */
    public ArrayQueue(int initialCapacity){
        @SuppressWarnings("Unchecked")
        T [] temp = (T[]) new Object[initialCapacity+1];
        this.queue = temp;
        this.frontIndex = 0;
        this.backIndex = initialCapacity;
    }// end Array Queue
    
    @Override
    public void enqueue(T newEntry) {
        this.ensureCapacity();
        this.backIndex = (this.backIndex+1)%queue.length;
        queue[this.backIndex] = newEntry;
    }// end enqueue

    @Override
    public T dequeue() {
        T front = null;
        if(!isEmpty()){
            front = this.queue[frontIndex];
            this.queue[frontIndex] = null;
            this.frontIndex = (this.frontIndex+1)%queue.length;
        }//end if
        return front;
    }// end dequeue

    @Override
    public T getFront() {
        T front = null;
        if(!this.isEmpty()) front = this.queue[this.frontIndex];
        return front;
    }// end getFront

    @Override
    public boolean isEmpty() {
        return (this.backIndex+1)%this.queue.length == this.frontIndex;
    }// end isEmpty

    @Override
    public void clear() {
        this.frontIndex = 0;
        this.backIndex = this.queue.length-1;
        for(int index = 0; index < this.queue.length; index++){
            this.queue[index] = null;
        }// end for 
    }// end clear
    
    // Doubles the size of the array if the queue is full
    private void ensureCapacity(){
        if(this.frontIndex == ((this.backIndex+2)%this.queue.length)){
            T [] oldQueue = this.queue;
            int oldSize = oldQueue.length;
            @SuppressWarnings("unchecked")
            T [] tempQueue = (T[]) new Object[2*oldSize];
            this.queue = tempQueue;
            for(int index = 0; index < oldSize-1; index++){
                this.queue[index] = oldQueue[frontIndex];
                this.frontIndex = (this.frontIndex+1)%oldSize;
            }// end for
            this.frontIndex = 0;
            this.backIndex = oldSize-2;
        }// end if
    }// end ensureCapacity
    
}
