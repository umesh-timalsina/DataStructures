package dataStructuresLab1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package DataStructuresLab;

import java.util.Scanner;
import java.util.Calendar;
/**
 *
 * @author utimalsina
 */
public class NamePrintingClass {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
        Calendar rightNow = Calendar.getInstance();
	int currentYear = rightNow.get(1);
	//System.out.println(currentYear);
	int birthYear;
        String name;
        System.out.println("Enter Your Birth Year");
        birthYear = sc1.nextInt();
        System.out.println("Enter your Name");
        name = sc2.nextLine();
        System.out.println("Your name is " + name +" and age is " + (currentYear-birthYear));
    }
}
