/*
 * Assignment 6 Question Number 1:
 * The method splice is created, since it cannot work on the object provided dir
 * ectly, a copy constructor is used to copy the queue to another queue.
 */
package assignment6;

/** A Class that provides linked implementation of the Queue Interface
 * @author robotics
 * @param <T> A generic object
 */
public class LinkedQueue<T> implements QueueInterface<T> {
    private Node firstNode;
    private Node LastNode;
    
    public LinkedQueue(){
        
    }// Overloading default costructor
    /* A Copy Constructor for this class */
    public LinkedQueue(LinkedQueue another){
        this.firstNode = another.firstNode;
        this.LastNode = another.LastNode;
    }// end linked queue
    
    @Override
    public void enqueue(T newEntry) {
        Node newNode = new Node(newEntry, null);
        if(this.firstNode==null){
            this.firstNode = newNode;
        }// end if
        else{
            this.LastNode.setNextNode(newNode);
        }// end else
        this.LastNode = newNode;
    }// end enqueue

    @Override
    public T dequeue() {
        T front = null;
        if(!this.isEmpty()){
            front = this.firstNode.getData();
            this.firstNode = this.firstNode.getNextNode();
            if (this.firstNode == null) this.LastNode = null;
        }// end if
        if (this.firstNode == null) this.LastNode = null;
        return front;
    }// end dequeue

    @Override
    public T getFront() {
        T front = null;
        if(!this.isEmpty()) front = this.firstNode.getData();
        return front;
    }// end getFront

    @Override
    public boolean isEmpty() {
        return this.firstNode==null;
    }// end isEmpty

    @Override
    public void clear() {
        this.firstNode = this.LastNode = null;
    }// end clear
    
    /** Prints all the elements of the queue */
    public void printQueue(){
        Node currentNode = this.firstNode;
        while(currentNode != null){
            System.out.print(currentNode.getData() + "   " );
            currentNode = (currentNode.getNextNode() );
        }// end while
        System.out.println("");
    }// end printQueue

    /** Adds all the entries of another queue to this queue, starting after the 
     * last entry of this queue
     * @param anotherQueue another queue to be added
     */
    public void splice(QueueInterface<T> anotherQueue){
        LinkedQueue<T> copyOf = new LinkedQueue((LinkedQueue<T>)anotherQueue);
        while(!copyOf.isEmpty()){
            this.enqueue(copyOf.dequeue());
        }// end while
    }// end splice
 
    private class Node{
        private T data;
        private Node nextNode;
        /* Constructor for the node class */
        public Node(T data, Node nextNode) {
            this.data = data;
            this.nextNode = nextNode;
        }//end Node
        
        /* A set of getter and setter methods for this class */
        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node getNextNode() {
            return nextNode;
        }

        public void setNextNode(Node nextNode) {
            this.nextNode = nextNode;
        }
    }
}
