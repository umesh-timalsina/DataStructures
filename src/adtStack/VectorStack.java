/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtStack;
import java.util.Vector;
/**
 *
 * @author robotics
 * @param <T>
 */
public class VectorStack<T> implements Stack<T> {
    private Vector<T> stack;
    private final static int DEFAULT_CAPACITY = 50;
    
    public VectorStack(){
        this(DEFAULT_CAPACITY);
    }
    public VectorStack(int initialCapacity){
        stack = new Vector<T>(initialCapacity);
    }
    
    @Override
    public void push(T newEntry){
        this.stack.add(newEntry);
    }
    
    @Override
    public boolean isEmpty(){
        return stack.isEmpty();
    }
    
    @Override
    public int size(){
        return this.stack.size();
    }
    
    @Override
    public T pop(){
        T top = null;
        if(!isEmpty()){
            top = stack.remove(stack.size()-1);
        }
        return top;
    }
    
    @Override
    public T top(){
        T top = null;
        if(!isEmpty()){
            top = stack.get(stack.size()-1);
        }
        return top;
    }
    
        public T [] toArray(){
        T [] temp = (T []) new Object[this.size()];
        int index = 0;
        int count = this.size();
        while(!this.isEmpty() && index < this.size()){
            temp[index] = this.pop();
            index++;
        }
        for(int i = 0; i < count; i++){
            this.push(temp[i]);
        }
        return temp;
    }
}
