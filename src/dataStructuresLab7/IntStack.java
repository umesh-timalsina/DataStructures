/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab7;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;
/** An integer stack containing several elements
 * @author utimalsina
 */
public class IntStack {
    public static void main(String[] args) {
        Stack<Integer> intStack = new Stack<>();
        Scanner sc1 = new Scanner(System.in);
        int ncount=10;
        System.out.println("Please enter 10 numbers");
        for(int i = 0; i < ncount; i++){
            intStack.push(sc1.nextInt());
        }
        Stack<Integer> reverseStack = new Stack<>();
        while(!intStack.isEmpty()){
            reverseStack.push(intStack.pop());
        }

        while(!reverseStack.isEmpty()){
            System.out.println(reverseStack.peek());
            intStack.push(reverseStack.pop());
        }
        Iterator<Integer> itr = reverseStack.iterator(); // create a new iterator 
                                                         // Over the stack
        while(itr.hasNext()){
            intStack.push(itr.next());
            System.out.println(itr.next());
        }
    }
}
