/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment1.Problem4;

/**
 *
 * @author tumesh
 */
public class ArrayBag<T> implements BagInterface<T> {

    private static final int DEFAULT_CAPACITY = 25;
    private int numberOfEntries;
    private T [] bag;
    
    /** Non Default constructor for the class ArrayBag
     * @param capacity is the number of entries in the bag
     */
    public ArrayBag(int capacity){
        this.numberOfEntries = 0;
        @SuppressWarnings("Unchecked")
        T [] tempBag = (T []) new Object[capacity];
        bag = tempBag;
    } // An array of type Object is created and a type cast is done
    
    /** The Default Constructor for the class ArrayBag.
     */
    public ArrayBag(){
        this(DEFAULT_CAPACITY);
    } // calling the parametric constructor with default capacity
    
    @Override
    public int getCurrentSize() {
       return this.numberOfEntries;
    }

    @Override
    public boolean isFull() {
        return this.numberOfEntries == this.bag.length;
    }

    @Override
    public boolean isEmpty() {
        return this.numberOfEntries == 0;
    }

    @Override
    public boolean add(T newEntry) {
        boolean result = true;
        if(this.isFull()){
            result = false;
        }
        else{
            this.bag[this.numberOfEntries] = newEntry;
            this.numberOfEntries++;
        }
        return result;
    }   

    @Override
    public T remove(T anEntry) {
        T result = null;
        int index;
        if(!(this.contains(anEntry))){
            return result;
        }
        else{
            index = getIndexOf(anEntry);
            result = this.bag[index];
            this.numberOfEntries--;
            this.bag[index] = this.bag[this.numberOfEntries];
            this.bag[this.numberOfEntries] = null;
        }
        return result;
    }
    private int getIndexOf(T anEntry){
        int where = -1;
        boolean stillLooking = true;
        for(int i = 0; stillLooking && i < this.numberOfEntries; i++){
            if(anEntry.equals(this.bag[i])){
                stillLooking = false;
                where = i;
            }
        }
        return where;
    }

    @Override
    public T remove() {
        T result = null;
        if(this.numberOfEntries > 0){
            this.numberOfEntries--;
            result = this.bag[this.numberOfEntries];
            //System.out.println("Removed " + result);
            this.bag[this.numberOfEntries]= null;
        }
        return result;
    }

    @Override
    public void clear() {
        for(int i = 0; i < this.bag.length; i++){
            this.bag[i] = null;
        }
    }

    @Override
    public int getFrequencyOf(T anEntry) {
        int counter = 0;
        for(int i=0; i < this.numberOfEntries; i++){
            if(anEntry.equals(this.bag[i]))
                counter++;
        }
        return counter;
    }

    @Override
    public boolean contains(T anEntry) {
        boolean found = false;
        for(int i = 0; !found && i < this.numberOfEntries; i++){
            if(anEntry.equals(this.bag[i]))
                found = true;
        }
        return found;
    }

    @Override
    public T[] toArray() {
        @SuppressWarnings("unchecked")
        T [] returnArray = (T []) new Object[this.numberOfEntries];
        //System.arraycopy(this.bag, 0, returnArray, 0, this.numberOfEntries);
        for(int index = 0; index < returnArray.length; index++){
            returnArray[index] = this.bag[index];
        }
        return (returnArray);
    }
    
    /** A method that removes all occurrences of a given entry from the bag.
     * @param anEntry is the given entry
     * @return true if the removal was successful, false otherwise
     */
//<editor-fold defaultstate="collapsed" desc="New Method removeEvery">
    public boolean removeEvery(T anEntry){
        int freq = this.getFrequencyOf(anEntry);
        boolean removed = false;
        for(int i = 0; i < freq; i++){
            this.remove(anEntry);
            removed = true;
        }
        return removed;
    }
//</editor-fold>
}
