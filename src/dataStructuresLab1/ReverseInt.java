package dataStructuresLab1;

import java.util.Scanner;
public class ReverseInt{

	public static void main(String [] args){
		Scanner sc1 = new Scanner(System.in);
		int num = sc1.nextInt();
		int reverseNumber=0,rem;
		do{
			rem = num%10;
			reverseNumber = reverseNumber*10+rem;
			num/=10;

		}while(num>0);
		System.out.println("The reverse number is " + reverseNumber);
	}
}
