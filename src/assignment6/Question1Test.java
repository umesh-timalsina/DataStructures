package assignment6;
public class Question1Test {
    public static void main(String[] args) {
        QueueInterface<String> queue1 = new LinkedQueue<>();
        queue1.enqueue("Lord Shiva");
        queue1.enqueue("Lord Vishnu");
        queue1.enqueue("Lord Bramha");
        queue1.enqueue("Lord Krishna");
        QueueInterface<String> queue2 = new LinkedQueue<>();
        queue2.enqueue("Godess Durga");
        queue2.enqueue("Godess Kali");
        queue2.enqueue("Godess Maheshwari");
        ((LinkedQueue<String>) queue1).splice(queue2);
        ((LinkedQueue<String>) queue1).printQueue();
    }
}
