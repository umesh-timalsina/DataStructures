/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2Scribbles;

/**
 *
 * @author robotics
 */
public class Test {
    
    public static void print(BagInterface bag1){
        Object [] arrBag = new Object[bag1.getCurrentSize()];
        arrBag = bag1.toArray();
        for(Object Entry : arrBag){
            System.out.println(Entry.toString());
        }
    }
    
    public static void main(String[] args) {
        SinglyLinkedList<String> bag1 = new SinglyLinkedList(); // Creates a new bag
        bag1.add("Hello");
        bag1.add("Darkness");
        bag1.add("My");
        bag1.add("Old");
        bag1.add("Friend");
        
        bag1.remove("Hello");
        Test.print(bag1);
        System.out.println(bag1.getCurrentSize());
        
        DoublyLinkedBag<String> bag2 = new DoublyLinkedBag<>();
        bag2.add("nepal");
        bag2.add("ko");
        bag2.add("choro");
        
        //Test.print(bag2);
        
        //System.out.println(bag2.getLastNode());
        System.out.println(bag2.getFrequencyOf("nepal"));
        //Test.print(bag2);
        bag2.clear();
        bag2.add("Last entry");
        bag2.add("Ma Naya Hun");
//        bag2.remove();
//        bag2.remove();
//        bag2.remove();
        System.out.println(bag2.getLastNode());
    }
}
