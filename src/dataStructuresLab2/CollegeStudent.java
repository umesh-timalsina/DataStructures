/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab2;

/**
 *
 * @author utimalsina
 */
public class CollegeStudent extends Student {
    private String degreeEnrolled;
    private int graduationYear;
    
    public void setStudent(Name nameIn, String studentID, String degreeEnrolled, int graduationYear){
        super.setStudent(nameIn, studentID);
        this.degreeEnrolled = degreeEnrolled;
        this.graduationYear = graduationYear;
    }

    public String getDegreeEnrolled() {
        return degreeEnrolled;
    }

    public void setDegreeEnrolled(String degreeEnrolled) {
        this.degreeEnrolled = degreeEnrolled;
    }

    public int getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(int graduationYear) {
        this.graduationYear = graduationYear;
    }
    
}
