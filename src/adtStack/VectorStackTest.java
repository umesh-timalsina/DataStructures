/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtStack;

/**
 *
 * @author robotics
 */
public class VectorStackTest {
    public static void main(String[] args) {
        VectorStack<Integer> vs1 = new VectorStack<>();
        for(int i = 0; i <= 100; i++){
            vs1.push(i);
        }
        while(!vs1.isEmpty()){
            System.out.println(vs1.pop());
        }
    }
}
