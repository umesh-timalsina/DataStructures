/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtStack2;
import adtStack.*;
/** This class has a static method named checkBalance that checks whether the 
 * given expression is balanced viz-a-viz parenthesis.
 * @author tumesh
 */
public class BalanceChecker {
    public static boolean checkBalance(String expression){
        Stack<Character> openDelimeterStack = new ArrayStack();
        boolean isBalanced=true;
        int index=0;
        int charCount=expression.length();
        Character nextChar;
        for(; isBalanced && index<charCount;index++){
            nextChar = expression.charAt(index);
            switch(nextChar){
                case '[': case '{': case '(':
                    openDelimeterStack.push(nextChar);
                    break;
                case ']': case '}': case ')':
                    if(openDelimeterStack.isEmpty()) {
                        isBalanced = false;
                        System.out.println("I am here");
                    }
                    else{
                        Character isDelimeter = openDelimeterStack.pop();
                        isBalanced = isPaired(isDelimeter, nextChar);
                    }
                    break;
                    
                default: break;
            }
        } // Check if the expression balances out
        
        return isBalanced;
    }
    //Checks whether two paranthesis close each other
    private static boolean isPaired(Character open, Character close){
        //System.out.println("Checking " + open + "With" + close);
        return((open == '(' && close == ')') ||
                (open == '{' && close == '}') ||
                (open == '[' && close == ']'));
    }
    public static void main(String[] args) {
        System.out.println(checkBalance("{A+(b*c)/4}"));
        System.out.println(isPaired('(', ')'));
    }
        
   
}
