/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursionScribbles;

/**
 *
 * @author robotics
 */
public class RecursionTest {

    public static void skipLines(int n){
        if(n > 0){
            System.out.println("");
            skipLines(n-1);
        }
        else System.out.println("Skipped lines. To the last now");
    }
    public static void main(String[] args) {
        skipLines(10);
    }
}
