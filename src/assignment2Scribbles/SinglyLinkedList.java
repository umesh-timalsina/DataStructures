/*
 * A class to implement the BagInterface using singlylinked Lists.
 * All pertinent methofs are overriden
 */
package assignment2Scribbles;

/**
 *
 * @author robotics
 * @param <T> Generic Objects
 */
public class SinglyLinkedList<T> implements BagInterface<T>{
    
    private Node firstNode;
    private Node currentNode;
    private int numberOfEntries = 0;
    
    
    @Override
    public boolean add(T newEntry){
        Node newNode = new Node(newEntry);
        newNode.setNext(firstNode);
        firstNode = newNode;
        this.numberOfEntries++;
        return true;
    }// Adding has never been easier
    
    @Override
    public int getCurrentSize(){
        return this.numberOfEntries;
    }
    
    @Override
    public T [] toArray(){
        this.currentNode = this.firstNode; // set the current node to first node
                                           // to iterate through entire chain
        @SuppressWarnings("unchecked")
        T [] temp = (T []) new Object[this.numberOfEntries]; // create a new array of objects
        int index = 0;
        while((index< this.numberOfEntries) && (currentNode != null)){
            temp[index] = currentNode.getData();
            index++;
            this.currentNode = this.currentNode.getNext();
        }
        return temp;
    }// Returns an array of object containing all the items in the bag
    
    @Override
    public boolean isEmpty(){
        boolean result=false;
        if(this.firstNode==null) result=true;
        return result;
    }// The list is empty if the first node points to null
    
    @Override
    public boolean isFull(){
        return false;
    }
    
    @Override
    public T remove(){
        T result = null;
        if(!this.isEmpty()){
            result = this.firstNode.getData();
            firstNode = firstNode.getNext();
            this.numberOfEntries--;
        }
        return result;
    }// remove the first element from the list
    
    @Override
    public boolean contains(T anEntry){
        boolean found=false;
        this.currentNode = this.firstNode;
        while(!found && currentNode != null){
            if(currentNode.getData().equals(anEntry)) found=true;
            currentNode = currentNode.getNext();
        }
        return found;
    }
    
    private Node getReferenceTo(T anEntry){
        Node match = null;
        boolean found = false;
        this.currentNode = this.firstNode;
        while(!found && this.currentNode!=null){
            if(this.currentNode.getData().equals(anEntry)) {
                match = currentNode;
                found = true;
            }
            else{
                this.currentNode = this.currentNode.getNext();
            }
        }
        return match;
    } // A helper method to get reference to the node contianing the entry
    
    @Override
    public boolean remove(T anEntry){
        boolean result = false;
        Node dest = getReferenceTo(anEntry);
        if(dest!=null){
            copyContentsOf(this.firstNode, dest);
            this.remove();
        }
        return result;
    }// Removing a specific item from the linked list
    
    /**A helper method to copy contents of one node to other
     * @param srcNode is the node whose contents is to be copied
     * @param destNode is the node whose contents is to be replaced
     */
    private void copyContentsOf(Node srcNode, Node destNode){
        destNode.setData(srcNode.getData());
    } // copying contents
    
    @Override
    public void clear(){
        while(!isEmpty()){
            remove();
        }
    }// Clearing using a simple technique
    
    @Override
    public int getFrequencyOf(T anEntry){
        int freq = 0;
        this.currentNode = this.firstNode;
        while(this.currentNode != null){
            if(this.currentNode.getData().equals(anEntry)) freq++;
            this.currentNode = this.currentNode.getNext();
        }
        return freq;
    } // Finding the number of times anEntry has occured in the bag.
    
    // Private class node for defining a singly linked list
    private class Node{
        private T data; // The data portion
        private Node next; // The pointer to the next node
        
        private Node(T data){
            this(data, null); 
        }
        private Node(T data, Node next){
            this.data = data;
            this.next = next;
        }

        private T getData() {
            return data;
        }

        private void setData(T data) {
            this.data = data;
        }

        private Node getNext() {
            return next;
        }

        private void setNext(Node next) {
            this.next = next;
        }    
    
    }
    
    
}
