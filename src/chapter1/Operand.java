/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter1;

/**
 *
 * @author tumesh
 */
public class Operand {
    private double opr1;
    private double opr2;

    public double getOpr1() {
        return opr1;
    }

    public void setOpr1(double opr1) {
        this.opr1 = opr1;
    }

    public double getOpr2() {
        return opr2;
    }

    public void setOpr2(double opr2) {
        this.opr2 = opr2;
    }
    
    
    
}
