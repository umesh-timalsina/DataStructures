/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructureslab6;

/**
 *
 * @author utimalsina
 */
public class ThreeSums {
    public static long sumMethod1(long n){
        long sum = 0;
        for(long i = 1; i <= n; i++){
            sum += i;
        }
        return sum;
    }
    public static long sumMethod2(long n){
        long sum = 0;
        for(long i = 1; i <= n; i++){
            for (long j = 1; j <= i; j++)
                sum += 1;
        }
        return sum;
    }
    
    public static long sumMethod3(long n){
        long sum = (n*(n+1)/2);
        return sum;
    }
    
    public static void main(String[] args) {
        long n = 10;
        long startTime, endTime, duration1, duration2, duration3;
        System.out.println("N \t\t Time for each method(nano seconds)");
        System.out.println("   \t\tSumMethod1 \tsumMethod2 \t   sumMethod3");
        for(long i = 0; i < 10; i++){
            startTime = System.nanoTime();
            sumMethod1(n);
            endTime = System.nanoTime();
            duration1 = (endTime-startTime); // this is the number of nano seconds
           
            startTime = System.nanoTime();
            sumMethod2(n);
            endTime = System.nanoTime();
            duration2 = (endTime-startTime); // this is the number of nano seconds
            
            startTime = System.nanoTime();
            sumMethod3(n);
            endTime = System.nanoTime();
            duration3 = (endTime-startTime); // this is the number of nano seconds
            
            System.out.printf("%-15d\t%-15d\t %-15d\t%-15d\n", n, duration1, duration2, duration3);
            //n = (long)Math.pow(n, 2);
            n = n*10;
        }
    }
}
