package dataStructuresLab1;

import java.util.Scanner;

public class FindMax{

	public static void main(String [] args){

		Scanner sc1 = new Scanner(System.in);
		System.out.println("Enter two numbers");
		int num1 = sc1.nextInt();
		int num2 = sc1.nextInt();
		int max = (num1>num2)?num1:num2;
		System.out.println("The maximum number is " + max);
	}

}
