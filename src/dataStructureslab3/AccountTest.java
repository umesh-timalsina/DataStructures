/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructureslab3;

/**
 *
 * @author tumesh
 */
public class AccountTest {
    public static void main(String[] args) {
        Account [] accounts = new Account[5];
        accounts[0] = new SAccount(250.0);
        accounts[1] = new CAccount(2500.0);
        accounts[2] = new SAccount(270.0);
        accounts[3] = new CAccount(3500.0);
        accounts[4] = new SAccount(1250.0);
        // Count the number of accounts in the array
        int sAccountsCounter =0 , cAccountsCounter = 0;
        System.out.println("S.N.  \t Account Type \t    Total Balance ");
        for(int i = 0; i < accounts.length ; i++){
            System.out.print( i+1 );
            if(accounts[i] instanceof SAccount){
                System.out.print('\t' + "Savings Account" + "\t\t");
                sAccountsCounter++;
            }// instanceof operator returns true if the object on the left belongs to the class on the right
            else{
                System.out.print("\tCurrent Account" + "\t\t");
                cAccountsCounter++;
            }
            System.out.print(accounts[i].totalBalance());
            System.out.println("");
            
        }
        System.out.println("Number of Accounts: " + (sAccountsCounter+cAccountsCounter));
        System.out.println("Number of Savings Accounts: " + sAccountsCounter);
        System.out.println("Number of Current Accounts: " + cAccountsCounter);
    }
}
