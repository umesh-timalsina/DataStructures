/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter1;

/**
 *
 * @author tumesh
 */
public class Calculator implements Operations{
    private Operand p1;
    
    public Calculator(){
        p1 = new Operand();
    }
    
    public void setValues(double a, double b){
        this.p1.setOpr1(a);
        this.p1.setOpr2(b);
    }
    
    @Override
    public double add(){
        return this.p1.getOpr1() + this.p1.getOpr2();
    }
    
    @Override
    public double sub(){
        return this.p1.getOpr1() - this.p1.getOpr2();
    }
    
    @Override
    public double mul(){
        return this.p1.getOpr1() * this.p1.getOpr2();
    }
    
    @Override
    public double div(){
        return this.p1.getOpr1()/ this.p1.getOpr2();
    }
    
}
