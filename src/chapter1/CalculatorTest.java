/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter1;

/**
 *
 * @author tumesh
 */
public class CalculatorTest {
    public static void main(String[] args) {
        Calculator c1 = new Calculator();
        c1.setValues(8, 9);
        double a = c1.add();
        double b = c1.sub();
        double c = c1.mul();
        double d = c1.div();
        
        System.out.format("%.2f \n%.2f \n%.2f \n%.2f\n", a, b, c, d);
        
    }
}
