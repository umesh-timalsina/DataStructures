/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructureslab3;

//import assignment1Scribbles.*;

/** An interface that describes the operations of a bag of objects
 * @author tumesh
 */
public interface BagInterface<T> {
    /** Gets the current number of entries in the bag.
     * @return the integer number of entries in the bag
     */
    public int getCurrentSize();
    /** Sees whether the bag is full.
     * @return true if the bag is full, false if not
     */
    public boolean isFull();
    /** Sees if the bag is empty.
     * @return true if the bag is empty, false if not
     */
    public boolean isEmpty();
    /** Adds a new entry to this bag.
     * @param newEntry the object to be added as a new entry to the bag.
     * @return true if the addition was successful, false if not
     */
    public boolean add(T newEntry);
    /** Remove one occurrence of the given entry from the bag.
     * @param anEntry the entry to be removed
     * @return true if the removal of the object was successful, false if not
     */
    public T remove(T anEntry);
    /** Remove one unspecified object from the bag, if possible
     * @return the removed entry if the removal was successful, or null
     */
    public T remove();
    /** Remove all the entries from the bag.  
     */
    public void clear();
    /** Counts the number of time a specific entry appears in the bag.
     * @param anEntry the object whose occurrence is to be counted
     * @return integer number of times anEntry appears in the bag
     */
    public int getFrequencyOf(T anEntry);
    /** Checks if an object is in the bag
     * @param anEntry is the object to be checked
     * @return true if anEntry is in the bag, false if not
     */
    public boolean contains(T anEntry);
    
    /** Creates an array containing all the entries that are in the bag.
     * @return a newly allocated array of all the entries in the bag
     */
    public T[] toArray();
    
} // end BagInterface
