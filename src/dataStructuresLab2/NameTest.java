/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab2;

/**
 *
 * @author utimalsina
 */
public class NameTest {
    
    public static void main(String [] args){
        // calling the default constructor
        Name name1 = new Name();
        
        //Use of Setter methods[Corresponds to question number two]
        name1.setFirst("Nazli");
        name1.setLast("Ansari");
        
        //Use of getter methods
        System.out.println(name1.getFirst());
        System.out.println(name1.getLast());
        
        //Declaring a student object
        Student s1 = new Student();
        
        // Setting name and ID accordingly
        s1.setStudent(name1, "IDK1100");
        // testing the get methods
        System.out.println(s1.getName());
        System.out.println(s1.getStudentID());
        // defining a new name for college student
        Name name2 = new Name();
        name2.setFirst("Umesh");
        name2.setLast("Timalsina");
        // creating an object of the class college student
        CollegeStudent s2 = new CollegeStudent();
        s2.setStudent(name2, "IDK1101", "MSCS", 2019);
        
        // testing getter and setter methods.
        System.out.println(s2.getName());
        System.out.println(s2.getDegreeEnrolled());
        System.out.println(s2.getGraduationYear());
    
    }

}
