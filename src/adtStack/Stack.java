/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adtStack;

/**
 *
 * @author tumesh
 * @param <T>
 */
public interface Stack<T> {
  
    /** Get the current number of entries in the stack
     * @return the current size of the stack 
     */
    public int size();
  
    /** Check to see if the stack is empty
     * @return true if the stack is empty, false otherwise
     */
    public boolean isEmpty();
    
    /** Check to see the top of the stack without removing and is equivalent to peek() 
     * @return the generic object T at the top of the stack
     * @throws StackEmptyException Throws an exception if the stack is empty
     */
    public T top() throws StackEmptyException;
    
    //modifier methods 

    /** Push an object to the stack.
     * @param element is the generic object to be pushed
     */
    public void push(T element);
    
    /** Perform a pop operation i.e. remove the top element of the stack
     * @return the removed top element from the stack
     * @throws StackEmptyException Throws an exception if the stack is empty 
     */
    public T pop() throws StackEmptyException;

}
