/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab9;

import java.util.ArrayDeque;
import java.util.NoSuchElementException;

/**
 *
 * @author utimalsina
 */
public class AnimalDequeTest {
    public static void main(String[] args) {
        ArrayDeque<String> animalDeque = new ArrayDeque(50);
        
        animalDeque.addFirst("Dog");
        animalDeque.addLast("Duck");
        System.out.printf("First element : %s Last Element : %s \n", 
                            animalDeque.getFirst(),
                            animalDeque.getLast()); // Prints the first and last element
        animalDeque.remove(); // remove the first element
        animalDeque.remove();
        System.out.println(animalDeque.poll());  // retunrs null
        try{
            animalDeque.remove();
        }// throws exception
        catch(NoSuchElementException noSuchElementException){
            System.err.println("No Element found");
            System.exit(1);
        }

        
                
    }
}
