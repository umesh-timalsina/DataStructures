package assignment1Scribbles;

/** An interface that defines operations of a wallet of objects 
 * @author tumesh
 */
public interface WalletInterface<T> {
    /** Gets the current total amount that is in the wallet.
     * @return the double value of amount in the wallet(Sum of the value of all
     * the notes in the wallet)
     */
    public double getTotalAmount();
 
    /** Gets information of all types of notes that are in this wallet.
     * @return a newly allocated array containing all the note and note objects
     * in the wallet
     */
    public T[] getAllNoteTypes();

    /** Check if the wallet is empty.
     * @return true if the wallet is empty, false otherwise
     */
    public boolean isEmpty();

    /** Check if a particular object is in the wallet.
     * @param  aNote is the object to be checked
     * @return true if one or more than one objects aNote exist in the wallet, 
     *         false otherwise
     */
    public boolean contains(T aNote);

    /** Add a compatible object to the wallet.
     * @param aNote is the compatible object to be added
     */
    public void addNote(T aNote);

    /** Remove a single note(compatible objects) from the wallet.
     * @param aNote is the object to be removed
     * @return the removed entry if the removal was successful, null otherwise
     */
    public T removeNote(T aNote);

    /** Reduce the total amount in the wallet.
     * @param billAmount is the amount to be reduced
     * @return true if the removal was successful, false otherwise
     */
    public boolean reduceTotalAmount(double billAmount);

    /** Remove everything from the wallet
     * @return a newly created array of object, containing everything from the wallet
     */
    public T[] clear();

    /** 
     * @param exchangeRate
     * @return
     */
    public double foriegnTotalAmount(double exchangeRate);

    /**
     *
     * @param amount
     */
    public void addAmount(double amount);
}
