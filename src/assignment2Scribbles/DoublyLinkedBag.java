/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2Scribbles;

/**
 *
 * @author robotics
 * @param <T>
 */
public class DoublyLinkedBag<T> implements BagInterface<T> {
    private Node firstNode;
    private Node lastNode;
    private Node currentNode;
    private int numberOfEntries = 0;


    public T getLastNode(){
        return this.lastNode.getData();
    }
    @Override
    public boolean add(T newEntry){
        Node newNode = new Node(newEntry);
        boolean result;
        if(this.firstNode == null){
            this.firstNode = newNode;
            this.lastNode = newNode;
            this.numberOfEntries++;
            result = true;
        }
        else{
            newNode.setNext(this.firstNode);
            this.firstNode.setPrev(newNode);
            this.firstNode = newNode;
            this.numberOfEntries++;
            this.currentNode = this.firstNode;
            while(this.currentNode.getNext() != null){
                this.currentNode = this.currentNode.getNext();
            }// This while loop has to do with finding the last node
            this.lastNode = this.currentNode;
            result = true;
        }
        return result;
    }
    
    @Override
    public int getCurrentSize(){
        return this.numberOfEntries;
    }
    
    @Override
    public T[] toArray(){
        @SuppressWarnings("Unchecked")
        T [] temp = (T [])new Object[this.numberOfEntries];
        int index = 0;
        this.currentNode = this.lastNode;
        while(index < this.numberOfEntries && this.currentNode != null ){
            temp[index] = this.currentNode.getData();
            currentNode = this.currentNode.getPrev(); 
            index++;
        }
        return temp;
    }//Reverse traversal

    @Override
    public boolean isFull() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        boolean result = false;
        if(this.firstNode == null) result = true;
        return result;
    }// Check to see if the first node points to null

    // A helper method to get reference to the node contianing the entry
    private Node getReferenceTo(T anEntry){
        Node match = null;
        boolean found = false;
        this.currentNode = this.firstNode;
        while(!found && this.currentNode!=null){
            if(this.currentNode.getData().equals(anEntry)) {
                match = currentNode;
                found = true;
            }
            else{
                this.currentNode = this.currentNode.getNext();
            }
        }
        return match;
    } 
    
    @Override
    public boolean remove(T anEntry) {
        boolean result = false;
        Node dest = this.getReferenceTo(anEntry);
        if(dest!=null){
            this.copyContentsOf(this.lastNode, dest);
            this.remove();
            result = true;
        }
        return result;
    }
    
        /**A helper method to copy contents of one node to other
     * @param srcNode is the node whose contents is to be copied
     * @param destNode is the node whose contents is to be replaced
     */
    private void copyContentsOf(Node srcNode, Node destNode){
        destNode.setData(srcNode.getData());
    } // copying contents
    

    @Override
    public T remove() {
        this.currentNode = firstNode;
        T result = null;
        if(this.firstNode == null) {
            System.out.println("List is empty");
        }
        else{
            //System.out.println("Removed " + this.current.getData());
            result = this.firstNode.getData();
            this.firstNode = this.currentNode.getNext();
            this.currentNode.setPrev(null);
        }
        return result;
    }// Removing an item from the first might be fun

    @Override
    public void clear() {
       this.currentNode = this.firstNode;
       while(this.currentNode != null){
            this.currentNode = this.currentNode.getNext();
            this.remove();
       }
    }

    @Override
    public int getFrequencyOf(T anEntry){
        int freq = 0;
        this.currentNode = this.firstNode;
        while(this.currentNode != null){
            if(this.currentNode.getData().equals(anEntry)) freq++;
            this.currentNode = this.currentNode.getNext();
        }
        return freq;
    } // Finding the number of times anEntry has occured in the bag.
    

    @Override
    public boolean contains(T anEntry){
        boolean found=false;
        this.currentNode = this.firstNode;
        while(!found && currentNode != null){
            if(currentNode.getData().equals(anEntry)) found=true;
            currentNode = currentNode.getNext();
        }
        return found;
    }

    
    // A private class node defines the base for defining the doubly linked list
    private class Node{
        private T data;
        private Node prev;
        private Node next;
        
        private Node(T data){
            this(data,null,null);
        }
        private Node(T data, Node prev, Node next){
            this.data = data;
            this.prev = prev;
            this.next = next;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node getPrev() {
            return prev;
        }

        public void setPrev(Node prev) {
            this.prev = prev;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
        
    }
}
