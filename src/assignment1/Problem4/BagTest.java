/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment1.Problem4;

/**
 *
 * @author tumesh
 */
public class BagTest {
    
    /** The main method for the class BagTest.  
     * @param args
     */
    public static void main(String[] args) {
        // letters Bag contains several letters in the english alphabet.
        ArrayBag<String> letters = new ArrayBag<>();
        String [] lettersToAdd = {"x", "y", "z", "a", "e", "j", "k", "m", 
                                    "q", "i", "m", "n", "k", "g"};
        for(String chr : lettersToAdd){
            letters.add(chr);
        } // Added characters from the array lettersToAdd
  
        // vowels Bag contains the five vowels in the english alphabet.
        ArrayBag<String> vowels = new ArrayBag<>(5);
        String [] vowelsToAdd = {"a", "e", "i", "o", "u"};
        for(String chr : vowelsToAdd){
            vowels.add(chr);
        }// Added vowels from the array vowelsToAdd
        
        
        ArrayBag<String> consonants = new ArrayBag<String>();
        int length = letters.getCurrentSize();
        for(int i = 0; i < length; i++){
            String chr = letters.remove();           
            if(!(vowels.contains(chr))){
               //System.out.println(chr);
               consonants.add(chr);
            }// Added only consonants to the bag consonants
        }// the bag consonanats is loaded with only consonanlts
        
        // Print the total number of consonants in the bag
        System.out.printf("The Total Number of Consonants in the bag : %d\n" 
                           , consonants.getCurrentSize());
        
        // Declare an array of Objects for toArray Method
//        Object [] consonantsArray2 = consonants.toArray();
        String [] consonantsArray2 = (String [])consonants.toArray();
        
        
        System.out.println("Each characters and Frequency in the array are ");
        System.out.println("Character" + "\t|" + "   "+" Frequency");
        for (String consonant : consonantsArray2) {
            System.out.println("----------------|------------------");
            System.out.printf("%s \t \t|\t %d \n", consonant.toString(), 
                       consonants.getFrequencyOf(consonant.toString()));
            
        } // This prints the frequency of each and every element in the bag. Red-
          // undant printing. Extra logic required to remove this.
    }
}
