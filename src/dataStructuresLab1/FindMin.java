package dataStructuresLab1;

import java.util.Scanner;

public class FindMin{

	public static void main(String [] args){

		Scanner sc1 = new Scanner(System.in);
		int [] num = new int[3];
		System.out.println("Enter three numbers");
		for(int i = 0;i<num.length; i++){
			num[i] = sc1.nextInt();
		}
		int min = num[0];
		for(int n : num){
			if(n<min){
				min = n;
			}
		}
		System.out.println("The minimum number is " + min);


	}

}
