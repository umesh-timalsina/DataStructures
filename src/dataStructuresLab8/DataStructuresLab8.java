
package dataStructuresLab8;

/**
 *
 * @author utimalsina
 */
public class DataStructuresLab8 {
    public static void main(String[] args) {
        System.out.println(calcInterest(1000, .10, 3));
//        System.out.println(returnOnInvestment(1000, .27, 200));
        System.out.println(repeatString("Go Bears!!", 2));
        System.out.println(rev("hi"));
        System.out.println(contains("Apple", 'p'));
        System.out.println(split(0, new int [] {1,1,0},0, 0, 0));
    }
    public static double calcInterest(double principal, 
                                        double interestRate,int numberOfYears){
        if(numberOfYears > 0)
            return (1+interestRate)*calcInterest(principal,
                                                 interestRate, numberOfYears-1);
        else 
            return principal;
    }// recursive method for calculating compound interest
    public static int returnOnInvestment(double amount, 
                                    double interestRate, double monthlyPayment){
        if((int)amount>0){
            double newAmount = ((1+interestRate/12) * amount) - monthlyPayment;
            return 1 + returnOnInvestment(newAmount, interestRate, monthlyPayment);
        }   
        else
            return 0;
    }// recursive method for calculating monthly interest
    
    public static String repeatString(String s, int i){
        if(i>0){
            return s+repeatString(s, i-1);
        }
        else{
            return "";
        }
    }//Method to repeat the string i times, recursively
    
    public static String rev(String s){
        if(s.length()<=0){
            return "";
        }
        else{
            char last = s.charAt(s.length()-1);
            s = s.substring(0, s.length()-1);
            return last+rev(s);
        }
    }// recursive method to print the reverse of a string in java
    
    public static boolean contains(String s, char k){
        if(s.length()>0){
            return ((s.charAt(s.length()-1))== k) || 
                                       contains(s.substring(0,s.length()-1), k);
        }
        else
            return false;
    } // method returns true if the string s contains character k
    
    public static boolean split(int index, int [] num1, int sum1, int sum2){
        if(index>=num1.length){
            return sum1==sum2;
        }
        if(split((index+1),num1,sum1+num1[index],sum2)){
            return true;
        }
        if(split((index+1),num1,sum1,sum2+num1[index])){
            return true;
        }
        return false;
    }// Check if the array is splitable recursively
    
    
}
