/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment6;

import java.util.Random;

/**
 *
 * @author robotics
 */
public class Question2Test {
    public static void main(String[] args) {
        ArrayPriorityQueue<MyInteger> apq = new ArrayPriorityQueue();
        Random r = new Random();
        MyInteger temp;
        for(int i = 0; i < 10; i++){
            temp = new MyInteger(i);
            apq.add(temp);
        }
        System.out.println("Size : " + apq.getSize());
        System.out.println(apq.remove());
        System.out.println(apq.remove());
        System.out.println("Size : " + apq.getSize());
    }
}
