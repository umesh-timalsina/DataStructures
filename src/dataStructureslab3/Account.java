/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructureslab3;

/**
 *
 * @author tumesh
 */
public interface Account {
    
    /** A method created for getting total balance of the account.
     * @return the total balance(double) of the account
     */
    public double totalBalance();
}
