/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab9;

/**
 *
 * @author utimalsina
 */
public class ComparableStrings implements Comparable<ComparableStrings>{
    private String value;

    public ComparableStrings( String valueIn) {
        this.value = valueIn;
    }

    public String getValue() {
        return value;
    }
    
    
   

    @Override
    public int compareTo(ComparableStrings o) {
        if(this.value.length() <= o.value.length() ){
            return 255;
        }
        else return -255;
    }// The priority is set according to this method. The longer the string
    
}
