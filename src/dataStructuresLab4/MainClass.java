/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab4;

/**
 *
 * @author utimalsina
 */
public class MainClass {
    public static void main(String[] args) {
        OnlineShoppingImplementation<String> shop1 = 
                                            new OnlineShoppingImplementation(5);
        String [] itemsAdded = {"Flowers", 
                                "Books", "Clothes", "Grocery", "Trips", "Sweets"};
        for( String s : itemsAdded){
            shop1.addItem(s);
        } // Adding item to the online shopping implementation
        System.out.println(shop1.getCurrentSize());
        // The current size has changed from initial capacity of 5
        shop1.removeItem(); // Removing the last item
        System.out.println(shop1.getCurrentSize());
        System.out.println("The bag contains \"Clothes\"");
        System.out.println(shop1.contains("Clothes"));
    }
}
