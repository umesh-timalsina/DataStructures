/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab9;
import java.util.Random;
/**
 *
 * @author utimalsina
 */
public class TestClass {
    public static void main(String[] args) {
        Airplane [] array = new Airplane[25];
        AirportQueue airportQueue = new AirportQueue();
        Random r = new Random();
        boolean airStatus;
        int randomNumber;
        for(int i = 0; i < 25; i++){
            array[i] = new Airplane();
            airStatus = (r.nextDouble() > 0.5);
            randomNumber = r.nextInt(1000) * 5;
            array[i].setIsInAir(airStatus);
            array[i].setFlightNumber(Integer.toString(randomNumber));
            airportQueue.addPlane(array[i]);
        }
        Airplane landedPlane = airportQueue.landPlane();
        System.out.println( "Flight Number "+ landedPlane.getFlightNumber() + " Just Landed");
    }
}

