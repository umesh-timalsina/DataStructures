/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab7;
import java.util.Scanner;
import java.util.Stack;

/** Test class for 
 * @author utimalsina
 */
public class Test {
    public static void main(String[] args) {
        Stack<Character> palindromeStack = new Stack();
        String userInput, reverseString;
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Please Enter the String");
        userInput = new String(sc1.nextLine());
        userInput = userInput.toUpperCase();
        int charCount = userInput.length();
        int index;
        char nextChar;
        for(index = 0; index < charCount; index++){
            nextChar = userInput.charAt(index);
            palindromeStack.push(nextChar);
        }
        reverseString = new String();
        while (!palindromeStack.isEmpty()) {
            reverseString = reverseString +  palindromeStack.pop();
        }
//        System.out.println(reverseString + "|" + userInput);
        
        if(reverseString.hashCode() == userInput.hashCode())
            System.out.println("It is a palindrome");
        else
            System.out.println("It is not a palindrome");
        
        
    }
}
