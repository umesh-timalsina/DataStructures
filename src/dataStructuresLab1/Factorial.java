package dataStructuresLab1;


import java.util.Scanner;
public class Factorial {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        int num;
	long fact=1;
        System.out.println("Enter a number");
        num = sc1.nextInt();
        for(int i = num; i>=1; i--){
            fact *= i; 
        }
        System.out.println("The factorial is " + fact);
    }
}
