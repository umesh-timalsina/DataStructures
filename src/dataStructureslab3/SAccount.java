/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructureslab3;

/**
 *
 * @author tumesh
 */
public class SAccount implements Account{
    private final static double INTEREST_RATE = .07;
    private double balance;
    
    public SAccount(double balance){
        this.balance = balance;
    }
    public SAccount(){
        this(0.0);
    }
    @Override
    public double totalBalance(){
        this.balance += this.balance*SAccount.INTEREST_RATE;
        return this.balance;
    }// Returns the balance in the SAccount object after adding interest
}
