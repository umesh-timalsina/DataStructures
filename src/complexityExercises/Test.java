/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package complexityExercises;

import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author robotics
 */
public class Test {
    
    public static void main(String[] args) {
        Date current = new Date();
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter a small value of n");
        int n = sc1.nextInt();
        int sum = 0;
        long start = System.currentTimeMillis();
        for(int i = 0; i <= n; i++){
            for(int j = 1; j <= 10000; j++){
                sum += j;
            }
        }
        long end = System.currentTimeMillis();
        long timeTaken = end-start;
        System.out.printf("The time it took : %d milliseconds \n", timeTaken);
        start = System.currentTimeMillis();
        for(int i = 0; i <= n; i++){
            for(int j = 1; j <= n; j++){
                sum += j;
            }
        }
        end = System.currentTimeMillis();
        timeTaken = end-start;
        System.out.printf("The time it took : %d milliseconds\n", timeTaken);

    }
}
