/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructuresLab9;

/**
 *
 * @author utimalsina
 */
public class Airplane {
    private boolean isInAir;
    private String flightNumber;
    
    public Airplane(){
    }
    
    public Airplane(boolean status){
        this.isInAir = status;
    }
    
    public boolean getIsInAir(){
        return this.isInAir;
    }
    
    public void setIsInAir(boolean status){
        this.isInAir = status;
    }
    
    public String getFlightNumber(){
        return this.flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
    
}
