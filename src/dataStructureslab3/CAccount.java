package dataStructureslab3;

/** A current Account class that implements the account interface
 * @author tumesh
 */
public class CAccount implements Account{
    private double balance;
    private static final double CHARGE = 0.08;
    
    
    public CAccount(double balance){
        this.balance = balance;
    }
    
    public CAccount(){
        this(0.0);
    }
    
    @Override
    public double totalBalance(){
        this.balance =  this.balance - this.balance * CAccount.CHARGE;
        return this.balance;
    }
}
